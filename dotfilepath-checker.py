#!/usr/bin/python

"""
Created on 2014-08-25

@author: rbone
"""

import sys
import getopt
import os
import re
import os.path
from xml.dom import minidom


pattern = re.compile('(\\w+\\.)+\\w+')
usage = 'usage: dotfilepath-checker.py -i <inputfile> -d <searchdir> (current dir if not specified)'


def main(argv):
    input_file = ''
    search_dir = "." + os.sep
    try:
        opts, args = getopt.getopt(argv, "hi:d:", ["inputfile=", "searchdir="])
    except getopt.GetoptError:
        print
        sys.exit(2)
    if len(opts) == 0:
        print usage
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print usage
            sys.exit()
        elif opt in ("-i", "--inputfile"):
            input_file = arg
        elif opt in ("-d", "--searchdir"):
            search_dir = arg
            if not search_dir.endswith(os.sep):
                search_dir += os.sep
    if input_file == '':
        print "Compulsory input file parameter was not given."
        print usage
        sys.exit(3)
    print 'Input file is "%s"' % input_file
    print 'Search directory is "%s"' % search_dir
    dom = minidom.parse(input_file)
    filepath_list = []
    parse_xml_node(dom, filepath_list)
    print "\n"
    if search_dir == "." + os.sep:
        print "Searching in directory: %s" % os.getcwd()
    else:
        print "Searching in directory: %s" % search_dir
    print "\n"
    for filepath in filepath_list:
        check_file_exists(search_dir + filepath)


def parse_xml_node(node, filepath_list):
    if node.hasChildNodes():
        for child in node.childNodes:
            parse_xml_node(child, filepath_list)
    else:
        # WTF is going on with this new line character i'm having to strip
        parent_data = node.data
        if parent_data.strip() != "":
            if pattern.match(parent_data) is not None:
                print "Filepath found in XML: %s" % parent_data
                replaced_with_separator = parent_data.replace(".", os.sep)
                index = replaced_with_separator.rfind(os.sep)
                formatted_string = replaced_with_separator[:index] + "." + replaced_with_separator[index + 1:]
                filepath_list.append(formatted_string)


def check_file_exists(filepath):
    if not os.path.isfile(filepath):
        print "File not found: %s" % filepath


if __name__ == "__main__":
    main(sys.argv[1:])